
// Mad Lib
// Devon Lozier, Devon Norman, Daniel Mader 

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;


void printMadLib(string arrMadLib[10], string arrAnswers[10])
{
	for (int i = 0; i < 10; i++)
	{
		cout << arrMadLib[i] << arrAnswers[i];
	}
}

void saveMadLib(string arrMadLib[], string arrAnswers[], ostream &os)
{
	for (int i = 0; i < 10; i++)
	{
		os << arrMadLib[i] << arrAnswers[i];
	}
}

int main()
{
	string madLibTypes[10]
	{
		"Silly Word", "Last Name", "Illness", "Noun (Plural)", "Adjective", "2nd Adjective", "Silly Word", "Place", "Number", "3rd Adjective"
	};
	string madLibAnswers[10];
	string madLib[10]{ 
		"Dear School Nurse:\n", 
		" ", 
		" will not be attending school today. He / she has come down with a case of ",
		" and has a horrible ",
		" and a/an ",
		" fever. \nWe have made an appointment with the ",
		" Dr. ",
		", who studied for many years in ",
		" and has ",
		" degrees in pediatrics. He will send you all the information you need. Thank you! \n\nSincerely,\nMrs."
	};
	
	for (int i = 0; i < 10; i++)
	{
		cout << "Enter a " << madLibTypes[i] << "\n";
		cin >> madLibAnswers[i];
	}

	printMadLib(madLib, madLibAnswers);

	string input;

	cout << "\n\nWould you like to save to the file?\n\nEnter Y to save or anything else to close." << "\n";
	cin >> input;
	if (input == "Y" || input == "y")
	{
		string filepath = "Mad-Lib.txt";
		ofstream ofs;
		ofs.open(filepath);
		if (ofs)
		{
			saveMadLib(madLib, madLibAnswers, ofs);
			cout << "Saved! ";
		}
		ofs.close();
		cout << "Press anything to close.";
	}

	(void)_getch();
	return 0;
}
